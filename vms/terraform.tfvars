# project name
projectname = "defender-lab-infra"

# OS image
sourceimage = "https://download.fedoraproject.org/pub/fedora/linux/releases/33/Cloud/x86_64/images/Fedora-Cloud-Base-33-1.2.x86_64.qcow2"

# the base image is the source image for all VMs created from it
baseimagediskpool = "default"


# domain and network settings
domainname  = "local"
networkname = "default" # default==NAT

subnets       = ["10.16.4.0/24", "10.16.5.0/24", "10.16.6.0/24"]
network_names = ["inner", "outer", "dmz"]
dhcp          = [false, false, false]


# host-specific settings
# RAM in bytes
# disk size in bytes (disk size must be greater than source image virtual size)
hosts = {
  "h_defender" = {
    name     = "h_defender",
    vcpu     = 1,
    memory   = "2048",
    diskpool = "default",
    disksize = "9370000000",
    mac      = "00:00:00:13:37:23",
    # sourceimage = "https://download.fedoraproject.org/pub/fedora/linux/releases/34/Cloud/x86_64/images/Fedora-Cloud-Base-34-1.2.x86_64.qcow2",
    sourceimage = "/var/lib/libvirt/images/Fedora-Cloud-Base-34-1.2.x86_64.qcow2",
    category    = "host-defender",
    network = {
      "dmz" = {
        name    = "dmz",
        mode    = "route",
        address = ["10.16.6.1"],
        mac     = "00:06:00:13:37:23",
      },
      "outer" = {
        name    = "outer",
        mode    = "route",
        address = ["10.16.5.1"],
        mac     = "00:05:00:13:37:23",
      },
    },
  },
}

